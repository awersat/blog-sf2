<?php

namespace Sibers\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Message
 *
 * @ORM\Table(name="message")
 * @ORM\Entity(repositoryClass="Sibers\AdminBundle\Repository\MessageRepository")
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many Messages have Many Users.
     * @ORM\ManyToMany(targetEntity="Sibers\BlogBundle\Entity\User", inversedBy="messages")
     * @ORM\JoinTable(name="cross_users_messages")
     */
    private $users;

    /**
     * @var int
     *
     * @ORM\Column(name="blog_id", type="integer", nullable=true)
     */
    private $blogId;

    /**
     * @var int
     *
     * @ORM\Column(name="comment_id", type="integer", nullable=true)
     */
    private $commentId;

    /**
     * One Message has One Blog.
     * @ORM\OneToOne(targetEntity="Sibers\BlogBundle\Entity\Blog", inversedBy="message")
     * @ORM\JoinColumn(name="blog_id", referencedColumnName="id", onDelete="cascade")
     */
    private $blog;

    /**
     * One Message has One Comment.
     * @ORM\OneToOne(targetEntity="Sibers\BlogBundle\Entity\Comment", inversedBy="message")
     * @ORM\JoinColumn(name="comment_id", referencedColumnName="id", onDelete="cascade")
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="processed", type="boolean", nullable=true)
     */
    private $processed;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set blogId
     *
     * @param integer $blogId
     *
     * @return Message
     */
    public function setBlogId($blogId)
    {
        $this->blogId = $blogId;

        return $this;
    }

    /**
     * Get blogId
     *
     * @return int
     */
    public function getBlogId()
    {
        return $this->blogId;
    }

    /**
     * Set commentId
     *
     * @param integer $commentId
     *
     * @return Message
     */
    public function setCommentId($commentId)
    {
        $this->commentId = $commentId;

        return $this;
    }

    /**
     * Get commentId
     *
     * @return int
     */
    public function getCommentId()
    {
        return $this->commentId;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Message
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Message
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set processed
     *
     * @param boolean $processed
     *
     * @return Message
     */
    public function setProcessed($processed)
    {
        $this->processed = $processed;

        return $this;
    }

    /**
     * Get processed
     *
     * @return bool
     */
    public function getProcessed()
    {
        return $this->processed;
    }

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * Add user
     *
     * @param \Sibers\BlogBundle\Entity\User $user
     *
     * @return Message
     */
    public function addUser(\Sibers\BlogBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \Sibers\BlogBundle\Entity\User $user
     */
    public function removeUser(\Sibers\BlogBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set blog
     *
     * @param \Sibers\BlogBundle\Entity\Blog $blog
     *
     * @return Message
     */
    public function setBlog(\Sibers\BlogBundle\Entity\Blog $blog = null)
    {
        $this->blog = $blog;

        return $this;
    }

    /**
     * Get blog
     *
     * @return \Sibers\BlogBundle\Entity\Blog
     */
    public function getBlog()
    {
        return $this->blog;
    }

    /**
     * Set comment
     *
     * @param \Sibers\BlogBundle\Entity\Comment $comment
     *
     * @return Message
     */
    public function setComment(\Sibers\BlogBundle\Entity\Comment $comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return \Sibers\BlogBundle\Entity\Comment
     */
    public function getComment()
    {
        return $this->comment;
    }
}
