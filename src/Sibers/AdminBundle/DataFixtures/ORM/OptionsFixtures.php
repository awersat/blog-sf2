<?php

namespace Sibers\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sibers\AdminBundle\Entity\Options;

/**
 * Description of OptionsFixtures
 *
 * @author dev
 */
class OptionsFixtures implements FixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $option = new Options();
        $option->setName('user-deleting-interval');
        $option->setTitle('Интервал удаления пользователя');
        $option->setValue(300);
        
        $manager->persist($option);
        $manager->flush();
        
    }

}
