<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06.01.17
 * Time: 15:22
 */

namespace Sibers\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sibers\AdminBundle\Entity\Message;


class MessageFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $messages = array();

        for ($i = 0; $i < 20; $i++) {
            $even = ($i % 2 == 0) ? true : false;
            $messages[$i] = new Message();
            $messages[$i]->setTitle("Cообщение-$i");
            $messages[$i]->setDescription("Разнообразный и богатый опыт реализация намеченных плановых заданий 
            способствует подготовки и реализации позиций, занимаемых участниками в отношении поставленных задач. 
            С другой стороны консультация с широким активом обеспечивает широкому кругу (специалистов) участие в 
            формировании системы обучения кадров, соответствует насущным потребностям");
            if ($even)
                $messages[$i]->addUser($this->getReference('user1'));
            $messages[$i]->addUser($this->getReference('user4'));
            if ($even)
                $messages[$i]->setComment($this->getReference('comment-' . $i));
            else
                $messages[$i]->setBlog($this->getReference('blog-' . $i));
            $manager->persist($messages[$i]);
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 5;
    }
}