<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06.01.17
 * Time: 17:25
 */

namespace Sibers\AdminBundle\Services;

use Sibers\BlogBundle\Entity\User;
use Sibers\BlogBundle\Entity\Blog;
use Sibers\BlogBundle\Entity\Comment;
use Sibers\AdminBundle\Entity\Message;

/**
 * Class Messager
 *
 * @package Sibers\AdminBundle\Services
 */
class Messager
{
    /**
     * Main ORM Service
     *
     * @var $doctrine
     */
    private $doctrine;
    private $router;

    /**
     * Messager constructor.
     * @param $doctrine
     */
    public function __construct($doctrine, $router)
    {
        $this->doctrine = $doctrine;
        $this->router = $router;
    }

    /**
     * @param Blog|null $blog
     * @param Comment|null $comment
     * @param User|null $user
     */
    public function createMessage(Blog $blog = null, Comment $comment = null, User $user = null)
    {
        $em = $this->doctrine->getManager();
        $admins = $em->getRepository('SibersBlogBundle:Group')->getUsersByGroupName('Администратор');

        $message = new Message();

        if ($comment) {
            $message->setTitle('Новый комментарий');
            $message->setDescription('Комментарий требует одобрения');
            $message->setComment($comment);
            if ($user) $message->addUser($user);
        } elseif ($blog) {
            $message->setTitle('Новый пост');
            $message->setDescription('Пост требует одобрения');
            $message->setBlog($blog);
        }
        if (is_array($admins)) {
            foreach ($admins as $admin) {
                $message->addUser($admin);
            }
        }
        $em->persist($message);
        $em->flush();

    }

    /**
     * @param $id
     */
    public function setProcessedStatus($id)
    {
        $em = $this->doctrine->getManager();
        $message = $em->getRepository('SibersAdminBundle:Message')->find($id);
        $message->setProcessed(true);
        $em->flush($message);
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function listAllNewMessage($userId)
    {

        $em = $this->doctrine->getManager();
        $messageRecipient = $em->getRepository('SibersBlogBundle:User')->find($userId);
        $messages = $messageRecipient->getMessages()->getValues();

        $result = array();
        $data = array();
        foreach ($messages as $key => $message) {
            if($key >= 15) break;

            if($message->getProcessed()) continue;

            if ($blog = $message->getBlog()) {
                $message->link = $this->router->generate('_show', array('id' => $blog->getId()));
            } elseif ($comment = $message->getComment()) {
                $message->link = $this->router->generate('comment_show', array('id' => $comment->getId()));
            }
            $data['id'] = $message->getId();
            $data['title'] = $message->getTitle();
            $data['description'] = $message->getDescription();
            $data['link'] = $message->link;

            $result[] = $data;


        } //TODO: Switch links on the admin actions
        return $result;

    }


}// end class