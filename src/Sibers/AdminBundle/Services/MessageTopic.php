<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12.01.17
 * Time: 10:24
 */

namespace Sibers\AdminBundle\Services;

use Gos\Bundle\WebSocketBundle\Topic\TopicInterface;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\Topic;
use Gos\Bundle\WebSocketBundle\Router\WampRequest;


class MessageTopic implements TopicInterface
{
    private $messager;

    /**
     * @param  ConnectionInterface $connection
     * @param  Topic $topic
     * @param WampRequest $request
     */
    public function onSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request)
    {
        $userId = $request->getAttributes()->get('user_id');
        $messages = $this->messager->listAllNewMessage($userId);
        $connection->event($topic->getId(), array('msg' => json_encode(['messages' => $messages])));

    }

    /**
     * @param  ConnectionInterface $connection
     * @param  Topic $topic
     * @param WampRequest $request
     */
    public function onUnSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request)
    {
        $userId = $request->getAttributes()->get('user_id');
        $topic->broadcast(array('msg' => "Пользователь $userId отключился"));
    }

    /**
     * @param  ConnectionInterface $connection
     * @param  Topic $topic
     * @param WampRequest $request
     * @param $event
     * @param  array $exclude
     * @param  array $eligible
     */
    public function onPublish(ConnectionInterface $connection, Topic $topic, WampRequest $request, $event, array $exclude, array $eligible)
    {
        $data = json_decode($event);
        $messId = (int)$data->message;
        $this->messager->setProcessedStatus($messId);
        $userId = $request->getAttributes()->get('user_id');
        $messages = $this->messager->listAllNewMessage($userId);
        $connection->event($topic->getId(), array('msg' => json_encode(['messages' => $messages])));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app.message.topic';
    }

    public function __construct($messager)
    {
        $this->messager = $messager;
    }
}