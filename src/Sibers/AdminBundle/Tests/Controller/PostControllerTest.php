<?php

namespace Sibers\AdminBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PostControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/post');
    }

    public function testEdit()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/post/edit');
    }

    public function testDelete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/post/delete');
    }

    public function testApprove()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/post/Approve');
    }

    public function testNew()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/post/new');
    }

}
