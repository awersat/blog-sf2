<?php

namespace Sibers\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PostController extends Controller
{
    /**
     * @Route("/post")
     */
    public function indexAction()
    {
        return $this->render('SibersAdminBundle:Post:index.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/post/edit")
     */
    public function editAction()
    {
        return $this->render('SibersAdminBundle:Post:edit.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/post/delete")
     */
    public function deleteAction()
    {
        return $this->render('SibersAdminBundle:Post:delete.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/post/Approve")
     */
    public function approveAction()
    {
        return $this->render('SibersAdminBundle:Post:approve.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/post/new")
     */
    public function newAction()
    {
        return $this->render('SibersAdminBundle:Post:new.html.twig', array(
            // ...
        ));
    }

}
