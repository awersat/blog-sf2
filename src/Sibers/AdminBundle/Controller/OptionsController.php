<?php

namespace Sibers\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sibers\AdminBundle\Entity\Options;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


/**
 * Admin options controller.
 *
 * @Route("options")
 */

class OptionsController extends Controller
{
    /**
     * @Route("/list", name="admin_list_options")
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();

        $options = $em->getRepository('SibersAdminBundle:Options')->findAll();
        return $this->render('SibersAdminBundle:Options:list.html.twig', array(
            'options' => $options
        ));
    }

    /**
     * @Route("/{id}/edit", name="admin_edit_options")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Options $option)
    {
        $form = $this->createForm('Sibers\AdminBundle\Form\OptionsType', $option, array(
            'action' => $this->generateUrl('admin_edit_options', array('id' => $option->getId()))
        ));
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return new Response('ok');
        }
        
        return $this->render('SibersAdminBundle:Options:edit.html.twig', array(
            'form' => $form->createView(),
            'option' => $option
                
        ));
    }

}
