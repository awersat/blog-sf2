<?php

namespace Sibers\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sibers\BlogBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


/**
 * Main admin controller.
 *
 * @Route("/")
 */
class MainController extends Controller
{
    const SWITCHED = 0; // Class constants
    const DELETED = 1;
    const DELAYED_REMOVAL = 2;
    
    /**
     * Main admin action
     * 
     * @Route("/", name="admin_index")
     */    
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('SibersBlogBundle:User')->createQueryBuilder('a');
        $query = $qb->getQuery();
        $paginator = $this->get('knp_paginator');
        $users = $paginator->paginate($query, $request->query->getInt('page', 1), 8);

        //$this->get('app.messager')->listAllNewMessage(521); //TODO: must be deleted

        return $this->render('SibersAdminBundle:Main:index.html.twig', array(
            'users' => $users
        ));
    }
    
    /**
     * Toggle user status
     * 
     * @Route("/{id}/toggle", name="admin_toggle_user")
     */
    public function toggleUserStatusAction(User $user)
    {
        $user->setEnabled($user->isEnabled() ? false : true);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array(
            'action' => self::SWITCHED,
            'usr_status' => $user->isEnabled()
        ));
        
    }
    
    /**
     * Set delayed removal user status
     * 
     * @Route("/{id}/delayed-removal", name="admin_delayed_removal_user")
     */
    public function delayedRemovalAction(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        if($user->isMustBeDeleted()){
            $user->setMustBeDeleted(null);
            $user->setEnabled(true);
        }else{
            $option = $em->getRepository('SibersAdminBundle:Options')->findOneByName('user-deleting-interval');
            $timestamp = time() + $option->getValue();
            $date = new \DateTime();
            $user->setMustBeDeleted($date->setTimestamp($timestamp));
            $user->setEnabled(false);
        }
        $em->flush($user);
        
        return new JsonResponse(array(
            'action' => self::DELAYED_REMOVAL,
            'delayed' =>  $user->isMustBeDeleted() 
                ? $date->format('Y-d-d H:i:s')
                : false
        ));
    }
    
    /**
     * Deletes a user entity.
     *
     * @Route("/{id}/delete", name="admin_delete_user")
     * @Method("DELETE")
     */
    public function deleteAction(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush($user); 
        return new JsonResponse(array(
            'action' => self::DELETED,
            'usr_status' => false
        ));
    }
    
    /**
     * Displays a form to edit an existing blog entity.
     *
     * @Route("/{id}/edit", name="admin_edit_user")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user)
    {
        $form = $this->createForm('Sibers\BlogBundle\Form\UserType', $user);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_index');
        }
        
        return $this->render('SibersAdminBundle:Main:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }
        
}
