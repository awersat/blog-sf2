<?php

namespace Sibers\AdminBundle\Command;

//use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class DelayedRemovalCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:delayed-removal')
                ->setDescription('Automatically removes tagged user');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
         $output->writeln('Delayed removal users');
         
         $em = $this->getContainer()->get('doctrine')->getManager();
         $query = $em->getRepository('SibersBlogBundle:User')
                 ->createQueryBuilder('c')
                 ->where('c.mustBeDeleted IS NOT NULL')
                 ->getQuery();
         $users = $query->getResult();
         $now = new \DateTime();
         $quantity = 0;
         foreach ($users as $user) {
             if($user->getMustBeDeleted() <= $now){
                 $em->remove($user);
                 $quantity++;
             }
         }
         $em->flush();
         
         $output->writeln("$quantity users successfully deleted!");
         
    }
}
