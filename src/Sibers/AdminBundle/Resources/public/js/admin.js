/*Admin JS file*/
var url;
var context;
var action;
var messages;
var result;

const SWITCHED = 0;
const DELETED = 1;
const DELAYED_REMOVAL = 2;

$(function () {
    $('.user-contol').click(function () {
        action = $(this).data('action');

        var question = '<p>Вы действительно хотите ';

        switch (action) {
            case 'toggle':
                question += $(this).find('span').hasClass('glyphicon-ok') === true
                    ? 'заблокировать'
                    : 'разблокировать';
                $('#ModalLabel').html('Блокировка пользователя');
                break;

            case 'delete':
                question += 'удалить';
                $('#ModalLabel').html('Удаление пользователя');
                break;

            case 'delay':
                question += $(this).find('span').hasClass('glyphicon-thumbs-down') === true
                    ? 'поставить в очередь на удаление'
                    : 'убрать из очерели на удаление';
                $('#ModalLabel').html('Отсроченное удаление пользователя');
                break;

        }
        question += ' этого пользователя?</p>';

        $('#modal-sm .modal-body').html(question
            + '<button type="button" class="btn btn-primary btn-toggle-usr">Ок</button>'
            + '<button type="button" class="btn btn-defaul btn-close" data-dismiss="modal">Отмена</button>');

        $('#modal-sm').modal({
            keyboard: false
        });

        url = $(this).attr('href');
        context = $(this);

        $('.btn-toggle-usr').click(function () {
            $.ajax({
                url: url,
                type: action === 'delete' ? 'DELETE' : 'GET',
                success: function (data) {
                    switch (data.action) {
                        case SWITCHED:
                            context.find('span')
                                .removeClass('glyphicon-ok glyphicon-remove')
                                .addClass(data.usr_status ? 'glyphicon-ok' : 'glyphicon-remove');
                            break;

                        case DELETED:
                            context.parents('tr').addClass('hidden');
                            break;

                        case DELAYED_REMOVAL:
                            context.find('span').removeClass('glyphicon-thumbs-up glyphicon-thumbs-down')
                            if (data.delayed) {
                                context.parents('tr').addClass('danger');
                                context.parent('.must-be-deleted').prepend('<div class="fire">'
                                    + '<span class="glyphicon glyphicon-fire"></span>'
                                    + data.delayed
                                    + '</div>'
                                );
                                context.find('span').addClass('glyphicon-thumbs-up');
                            } else {
                                context.parents('tr').removeClass('danger');
                                context.parent('.must-be-deleted').find('.fire').remove();
                                context.find('span').addClass('glyphicon-thumbs-down');
                            }
                            break;
                    }
                    $('#modal-sm').modal('hide');
                },
                dataType: 'json'

            });
            return false;
        });

        return false;
    });

    $('#modal-sm').on('hidden.bs.modal', function () {
        $('#modal-sm').data('bs.modal', false);
    });

    $('#modal-sm').on('loaded.bs.modal', function () {
        $('form.option-edit-form').submit(submitOptionForm);
    });
    /**
     * Web socket functions
     */
    var webSocket = WS.connect(WS_URI);
    webSocket.on("socket/connect", function (session) {
        console.log("Successfully Connected!"); //TODO: must be removed
        var data;
        session.subscribe(channel, function (uri, payload) {
            try {
                data = JSON.parse(payload.msg);
                if (data.messages && (data.messages instanceof Array) ) {
                    messages = data.messages;
                    displayMessages(session);
                }
                else {//TODO: must be removed
                    console.log("Received message: ", payload.msg);
                }
            }
            catch (e) {
                console.log(result = payload.msg);
            }



        });

        //session.publish(channel, "Обратная связь");

    });

    webSocket.on("socket/disconnect", function (error) {
        console.log("Disconnected for " + error.reason + " with code " + error.code);
    });
    //end Web socket functions

});

/**
 * Submit options form
 * @returns {boolean}
 */
function submitOptionForm() {
    $.ajax({
        url: $(this).attr('action'),
        type: $(this).attr('method'),
        data: $(this).serialize(),
        dataType: "html",
        success: function (data) {
            if (data === 'ok') {
                $('#modal-sm').modal('hide');
            } else {
                $('#modal-sm .modal-content').html(data);
                $('form.option-edit-form').submit(submitOptionForm);
            }
        }

    });

    return false;
}

/**
 *  Parse messages array and display messages
 */
function displayMessages(session) {
    var message;
    var count = messages.length;
    if (messages.length > 0) {
        $('.count-messages').html(messages.length).show();
    } else {
        $('.count-messages').hide();
    }
    $('.messages').html('');
    messages.forEach(function (item, i) {
        message = '<li>'
            + '<a href="#00" class="show-message" data-id="'
            + i
            + '">'
            + item.title
            + '</a></li>';
        $('.messages').append(message);
    });

    $('.show-message').click(function(){
        var index = $(this).data('id');
        $('#ModalLabel').html(messages[index].title);
        $('#modal-sm .modal-body').html(
            messages[index].description + '<br/>'
            + '<a href="'
            +  messages[index].link
            + '">Перейти</a>'
        );

        $('#modal-sm').modal({
            keyboard: false
        });

        session.publish(channel, JSON.stringify({'message': messages[index].id}));
        return false;
    });
}
