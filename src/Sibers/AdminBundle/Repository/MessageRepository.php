<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10.01.17
 * Time: 14:35
 */

namespace Sibers\AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;

class MessageRepository extends EntityRepository
{
    public function findNewMessages()
    {
        $qb = $this->createQueryBuilder('c')->select('c')
            ->where('c.processed IS NULL')
            ->setMaxResults(10);
        return $qb->getQuery()->getResult();
    }

    public function findNewMessagesByUser($id){
        $qb = $this->createQueryBuilder('c')->select('m')
            ->from('Sibers\AdminBundle\Entity\Message', 'm')
            ->innerJoin('Sibers\BlogBundle\Entity\User', 'u', 'WITH', 'u.id = :id')
            ->where('m.processed IS NULL')
            ->setParameter('id', $id)
            ->setMaxResults(10)
        ;
        return $qb->getQuery()->getResult();
    }


}