<?php

namespace Sibers\AdminBundle\Twig;

class RoleExtension extends \Twig_Extension
{

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('role', array($this, 'roleFilter')),
        );
    }

    public function roleFilter($role)
    {
        switch ($role) {
            case 'ROLE_USER':
                return 'Пользователь';
            case 'ROLE_BLOGER':
                return 'Блогер';
            case 'ROLE_ADMIN':
                return 'Администратор';
        }
    }
    
    public function getName()
    {
        return 'role_extension';
    }

}
