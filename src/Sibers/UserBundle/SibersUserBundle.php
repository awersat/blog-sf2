<?php

namespace Sibers\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SibersUserBundle extends Bundle
{

    public function getParent()
    {
        return 'FOSUserBundle';
    }

}
