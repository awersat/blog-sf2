<?php

namespace Sibers\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class CommentType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('body', null, array('label' => 'Ваш комментарий'))
                ->add('blog', null, array(
                    'class' => 'SibersBlogBundle:Blog',
                    'placeholder' => false,
                    'query_builder' => function(EntityRepository $er) {
                        $blogId = filter_input(INPUT_GET, 'blog');
                        $qb = $er->createQueryBuilder('c');
                        if ($blogId) {
                            $qb = $qb->where('c.id = :id')->setParameter('id', $blogId);
                        }
                        return $qb;
                    }
                ))
                ->add('user', null, array(
                    'class' => 'SibersBlogBundle:User',
                    'placeholder' => false,
                    'query_builder' => function(EntityRepository $er) {
                        $userId = filter_input(INPUT_GET, 'user');
                        $qb = $er->createQueryBuilder('c');
                        if ($userId) {
                            $qb = $qb->where('c.id = :id')->setParameter('id', $userId);
                        }
                        return $qb;
                    }
        ));
        if (filter_input(INPUT_GET, 'parent')) {
            $builder->add('parent', null, array(
                'class' => 'SibersBlogBundle:Comment',
                'placeholder' => false,
                'query_builder' => function(EntityRepository $er) {
                    $parentId = filter_input(INPUT_GET, 'parent');
                    $qb = $er->createQueryBuilder('c');
                    if ($parentId) {
                        $qb = $qb->where('c.id = :id')->setParameter('id', $parentId);
                    }
                    return $qb;
                }
            ));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sibers\BlogBundle\Entity\Comment'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sibers_blogbundle_comment';
    }

}
