<?php

namespace Sibers\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Sidebar controller.
 *
 * @Route("sidebar")
 */
class SidebarController extends Controller
{

    /**
     * @Route("/", name="sidebar_tagcloud")
     */
    public function tagCloudAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tags = $em->getRepository('SibersBlogBundle:Blog')->getTags();

        $tagWeights = $em->getRepository('SibersBlogBundle:Blog')
                ->getTagWeights($tags);
        return $this->render('SibersBlogBundle:Sidebar:tag_cloud.html.twig'
                , array('tags' => $tagWeights));
    }

    /**
     * @Route("/", name="sidebar_latest_comments")
     */
    public function latestCommentsAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $commentLimit = $this->container
                ->getParameter('comments.latest_comment_limit');
        $latestComments = $em->getRepository('SibersBlogBundle:Comment')
                ->getLatestComments($commentLimit);
        return $this->render('SibersBlogBundle:Sidebar:latest_comments.html.twig'
                , array('latestComments' => $latestComments));
    }

}
