<?php

namespace Sibers\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * About controller.
 *
 * @Route("about")
 */
class AboutController extends Controller
{

    /**
     * About actions
     * 
     * @Route("/", name="about_index")
     */
    public function indexAction()
    {
        return $this->render('SibersBlogBundle:About:index.html.twig', array(
        ));
    }

}
