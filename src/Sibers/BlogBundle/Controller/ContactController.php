<?php

namespace Sibers\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Contact controller.
 *
 * @Route("contact")
 */
class ContactController extends Controller
{

    /**
     * @Route("/", name="contact_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $form = $this->createContactForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $message = \Swift_Message::newInstance()
                    ->setFrom($data['email'])
                    ->setTo($this->container->getParameter('admin.email'))
                    ->setSubject('Обратная связь')
                    ->setBody(
                    $this->renderView(
                            'SibersBlogBundle:Contact:email.html.twig'
                            , array('data' => $data))
                    , 'text/html'
            );
            $this->get('mailer')->send($message);
            unset($form);
            $form = $this->createContactForm();
            $this->get('session')->getFlashBag()->add(
                'notice',
                'Ваше сообщение успешно отправлено!'
            );
            
        }

        return $this->render('SibersBlogBundle:Contact:index.html.twig', array(
                    'form' => $form->createView()
        ));
    }

    private function createContactForm()
    {
        return $this->createFormBuilder()
                    ->setAction($this->generateUrl('contact_index'))
                    ->setMethod('POST')
                    ->add('name', TextType::class, array('label' => 'Ваше имя'))
                    ->add('email', EmailType::class, array('label' => 'Ваш email'))
                    ->add('subject', TextType::class, array('label' => 'Тема сообщения'))
                    ->add('body', TextareaType::class, array('label' => 'Текст сообщения'))
                    ->getForm();
    }

}
