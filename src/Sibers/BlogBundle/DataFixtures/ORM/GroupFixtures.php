<?php

namespace Sibers\BlogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sibers\BlogBundle\Entity\Group;

/**
 * Description of GroupFixtures
 *
 * @author dev
 */
class GroupFixtures extends AbstractFixture implements OrderedFixtureInterface
{

   public function load(ObjectManager $manager)
   {
       $group1 = new Group('Пользователь', array('ROLE_USER'));
       $this->addReference('usr', $group1);
       $manager->persist($group1);
       
       $group2 = new Group('Блогер', array('ROLE_BLOGER'));
       $this->addReference('bloger', $group2);
       $manager->persist($group2);
       
       $group3 = new Group('Администратор', array('ROLE_ADMIN'));
       $this->addReference('admin', $group3);
       $manager->persist($group3);
             
       $manager->flush();
               
   }
   
   public function getOrder()
    {
        return 1;
    }
}
