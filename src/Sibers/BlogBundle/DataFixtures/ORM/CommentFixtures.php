<?php

namespace Sibers\BlogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sibers\BlogBundle\Entity\Comment;

/**
 * Description of CommentFixtures
 *
 * @author dev
 */
class CommentFixtures extends AbstractFixture implements OrderedFixtureInterface
{

    private $manager;
    private $commentBody = 'Равным образом новая модель организационной '
            . 'деятельности способствует подготовки и реализации форм '
            . 'развития. Задача организации, в особенности же постоянное '
            . 'информационно-пропагандистское обеспечение нашей деятельности '
            . 'позволяет оценить значение форм развития. Равным образом '
            . 'рамки и место обучения кадров требуют от нас анализа системы '
            . 'обучения кадров, соответствует насущным потребностям. '
            . 'Значимость этих проблем настолько очевидна, '
            . 'что реализация намеченных плановых заданий требуют от нас '
            . 'анализа форм развития.';

    public function load(ObjectManager $manager)
    {
        $comments = array();
        $this->manager = $manager;

        for ($i = 0; $i < 5; $i++) {
            $comments[$i] = $this->createComment($this->commentBody, 'user2', 'blog-0', ($i > 1) ? $comments[$i - 1] : null);
        }
        $len = count($comments);
        for ($i = $len; $i < $len+5; $i++) {
            $comments[$i] = $this->createComment($this->commentBody, ($i%2 == 0) ? 'user3' : 'user2', 'blog-3', ($i > $len+1) ? $comments[$i - 1] : null);
        }
        $len = count($comments);
        for ($i = $len; $i < $len+45; $i++) {
            $comments[$i] = $this->createComment($this->commentBody, ($i%2 == 0) ? 'user3' : 'user2', 'blog-0', ($i > $len+10) ? $comments[$i - 1] : null);
        }
        $len = count($comments);
        for ($i = 0; $i < $len; $i++) {
            $this->addReference('comment-' . $i, $comments[$i]);
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 4;
    }

    private function createComment($body, $user, $blog, $parent = null)
    {
        $comment = new Comment();
        $comment->setBody($body);
        $comment->setUser($this->getReference($user));
        $comment->setBlog($this->getReference($blog));
        if ($parent) {
            $comment->setParent($parent);
        }
        $comment->setCreated(new \DateTime());
        $comment->setUpdated($comment->getCreated());
        $comment->setApproved(true);
        $this->manager->persist($comment);
        return $comment;
    }

}
