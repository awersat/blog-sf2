<?php

namespace Sibers\BlogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sibers\BlogBundle\Entity\Blog;

/**
 * Description of BlogFixtures
 *
 * @author dev
 */
class BlogFixtures extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $blogs = array();

        for ($i = 0; $i < 60; $i++) {
            $blogs[$i] = new Blog();
            $blogs[$i]->setTitle("Блог № $i");
            $blogs[$i]->setBody('Tоварищи! сложившаяся структура организации '
                    . 'позволяет оценить значение позиций, занимаемых '
                    . 'участниками в отношении поставленных задач. Равным '
                    . 'образом консультация с широким активом позволяет '
                    . 'выполнять важные задания по разработке форм развития.'
                    . 'Идейные соображения высшего порядка, а также реализация'
                    . ' намеченных плановых заданий влечет за собой процесс '
                    . 'внедрения и модернизации модели развития. Равным образом '
                    . 'реализация намеченных плановых заданий влечет за собой '
                    . 'процесс внедрения и модернизации новых предложений.'
                    . 'Не следует, однако забывать, что постоянное '
                    . 'информационно-пропагандистское обеспечение '
                    . 'нашей деятельности способствует подготовки и реализации '
                    . 'существенных финансовых и административных условий. '
                    . 'Значимость этих проблем настолько очевидна, что рамки '
                    . 'и место обучения кадров в значительной степени '
                    . 'обуславливает создание систем массового участия. '
                    . 'Не следует, однако забывать, что сложившаяся '
                    . 'структура организации в значительной степени '
                    . 'обуславливает создание новых предложений. Повседневная '
                    . 'практика показывает, что дальнейшее развитие различных '
                    . 'форм деятельности обеспечивает широкому кругу '
                    . '(специалистов) участие в формировании модели развития.'
                    . 'Задача организации, в особенности же сложившаяся '
                    . 'структура организации в значительной степени '
                    . 'обуславливает создание форм развития. Задача организации, '
                    . 'в особенности же новая модель организационной '
                    . 'деятельности в значительной степени обуславливает '
                    . 'создание дальнейших направлений развития. Задача '
                    . 'организации, в особенности же консультация с широким '
                    . 'активом способствует подготовки и реализации позиций, '
                    . 'занимаемых участниками в отношении поставленных задач. '
                    . 'Товарищи! дальнейшее развитие различных форм деятельности '
                    . 'обеспечивает широкому кругу (специалистов) участие в '
                    . 'формировании дальнейших направлений развития. '
                    . 'Таким образом укрепление и развитие структуры играет '
                    . 'важную роль в формировании дальнейших направлений '
                    . 'развития. Повседневная практика показывает, что '
                    . 'постоянный количественный рост и сфера нашей активности '
                    . 'в значительной степени обуславливает создание систем '
                    . 'массового участия.');
            $blogs[$i]->setImage(($i<10 ? $i+1 : 10) . '.jpg');
            $blogs[$i]->setCreated(new \DateTime());
            $blogs[$i]->setUpdated($blogs[$i]->getCreated());
            $blogs[$i]->setUser($this->getReference(($i%2 == 0) ? 'user1' : 'user4'));
            $blogs[$i]->setTags('Блог, Symfony, Красивые места '
                    . (($i == 3 ) ? ', Тег 1' : ', Красота')
                    . ((($i%6) == 0) ? ', Мега тег' : ', Природа')
                    );
            $this->addReference('blog-' . $i, $blogs[$i]);
            $manager->persist($blogs[$i]);            
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }

}
