<?php

namespace Sibers\BlogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sibers\BlogBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Description of UserFixtures
 *
 * @author dev
 */
class UserFixtures extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $factory = $this->container->get('security.encoder_factory');

        $user1 = new User();
        $user1->setUsername('user1');
        $user1->setEmail('user2@test.com');
        $encoder = $factory->getEncoder($user1);
        $password = $encoder->encodePassword('test-pass', $user1->getSalt());
        $user1->setPassword($password);
        $user1->setFirstName('Ryan');
        $user1->setLastName('Tiguan');
        $user1->addGroup($this->getReference('bloger'));
        $user1->setEnabled(true);
        $this->addReference('user1', $user1);
        $manager->persist($user1);

        $user2 = new User();
        $user2->setUsername('user2');
        $user2->setEmail('user2@ftest.com');
        $encoder = $factory->getEncoder($user2);
        $password = $encoder->encodePassword('password', $user2->getSalt());
        $user2->setPassword($password);
        $user2->setFirstName('Александр');
        $user2->setLastName('Иванов');
        $user2->addGroup($this->getReference('usr'));
        $user2->setEnabled(true);
        $this->addReference('user2', $user2);
        $manager->persist($user2);

        $user3 = new User();
        $user3->setUsername('user3');
        $user3->setEmail('user3@test.com');
        $encoder = $factory->getEncoder($user3);
        $password = $encoder->encodePassword('qwerty', $user3->getSalt());
        $user3->setPassword($password);
        $user3->setFirstName('Иван');
        $user3->setLastName('Александров');
        $user3->addGroup($this->getReference('usr'));
        $user3->setEnabled(true);
        $this->addReference('user3', $user3);
        $manager->persist($user3);

        $user4 = new User();
        $user4->setUsername('admin');
        $user4->setEmail('admin@test.com');
        $encoder = $factory->getEncoder($user4);
        $password = $encoder->encodePassword('admin', $user4->getSalt());
        $user4->setPassword($password);
        $user4->setFirstName('Admin');
        $user4->setEnabled(true);
        $user4->setLastName('Adminovich');
        $user4->addGroup($this->getReference('admin'));
        $this->addReference('user4', $user4);
        $manager->persist($user4);

        $users = array();
        for ($i = 0; $i < 100; $i++) {
            $users[$i] = new User();
            $users[$i]->setUsername("user-$i");
            $users[$i]->setEmail("user-$i@test.com");
            $encoder = $factory->getEncoder($users[$i]);
            $password = $encoder->encodePassword("pass-$i", $users[$i]->getSalt());
            $users[$i]->setPassword($password);
            $users[$i]->setFirstName("Юзверь-$i");
            $users[$i]->setEnabled(true);
            $users[$i]->setLastName('Тестович');
            $users[$i]->addGroup($this->getReference('usr'));
            $this->addReference("user-$i", $users[$i]);
            $manager->persist($users[$i]);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }

}
