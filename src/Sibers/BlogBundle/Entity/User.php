<?php

namespace Sibers\BlogBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * User
 * @ORM\Entity(repositoryClass="Sibers\BlogBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 * 
 */
class User extends BaseUser
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="Group", inversedBy="users")
     * @ORM\JoinTable(name="cross_user_group")
     */
    protected $groups;

    /**
     * Many Users have Many Messages.
     * @ORM\ManyToMany(targetEntity="Sibers\AdminBundle\Entity\Message", mappedBy="users")
     */
    protected $messages;

      /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     */
    private $lastName;
    
    /**
     * Get id
     *
     * @return int
     */
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="must_be_deleted", type="datetime", nullable=true)
     */
    private $mustBeDeleted;
    
    public function getId()
    {
        return $this->id;
    }
    

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @ORM\OneToMany(targetEntity="Blog", mappedBy="user", cascade={"all"}) 
     */
    protected $blogs;
    
    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="user", cascade={"all"})
     */
    protected $comments;

    public function __construct()
    {
        parent::__construct();
        $this->blogs = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->groups = new ArrayCollection();
    }


    /**
     * Add blog
     *
     * @param \Sibers\BlogBundle\Entity\Blog $blog
     *
     * @return User
     */
    public function addBlog(\Sibers\BlogBundle\Entity\Blog $blog)
    {
        $this->blogs[] = $blog;

        return $this;
    }

    /**
     * Remove blog
     *
     * @param \Sibers\BlogBundle\Entity\Blog $blog
     */
    public function removeBlog(\Sibers\BlogBundle\Entity\Blog $blog)
    {
        $this->blogs->removeElement($blog);
    }

    /**
     * Get blogs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBlogs()
    {
        return $this->blogs;
    }
    
    
    public function __toString()
    {
        return "$this->firstName $this->lastName";
    }

    /**
     * Add comment
     *
     * @param \Sibers\BlogBundle\Entity\Comment $comment
     *
     * @return User
     */
    public function addComment(\Sibers\BlogBundle\Entity\Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \Sibers\BlogBundle\Entity\Comment $comment
     */
    public function removeComment(\Sibers\BlogBundle\Entity\Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set mustBeDeleted
     *
     * @param \DateTime $mustBeDeleted
     *
     * @return User
     */
    public function setMustBeDeleted($mustBeDeleted)
    {
        $this->mustBeDeleted = $mustBeDeleted;

        return $this;
    }

    /**
     * Get mustBeDeleted
     *
     * @return \DateTime
     */
    public function getMustBeDeleted()
    {
        return $this->mustBeDeleted;
    }
    
    public function isMustBeDeleted()
    {
        return (bool) $this->mustBeDeleted;
    }
    

    /**
     * Add message
     *
     * @param \Sibers\AdminBundle\Entity\Message $message
     *
     * @return User
     */
    public function addMessage(\Sibers\AdminBundle\Entity\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \Sibers\AdminBundle\Entity\Message $message
     */
    public function removeMessage(\Sibers\AdminBundle\Entity\Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }
}
