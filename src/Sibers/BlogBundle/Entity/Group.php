<?php

namespace Sibers\BlogBundle\Entity;

use FOS\UserBundle\Model\Group as BaseGroup;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Group
 *
 * @ORM\Entity(repositoryClass="Sibers\BlogBundle\Repository\GroupRepository")
 * @ORM\Table(name="usr_group")
 * 
 */
class Group extends BaseGroup
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="User", mappedBy="groups")
     *
     */
    private $users;

    public function __construct($name, array $roles = array())
    {
        parent::__construct($name, $roles);
        $this->users = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }


    /**
     * Add user
     *
     * @param \Sibers\BlogBundle\Entity\User $user
     *
     * @return Group
     */
    public function addUser(\Sibers\BlogBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \Sibers\BlogBundle\Entity\User $user
     */
    public function removeUser(\Sibers\BlogBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
}
