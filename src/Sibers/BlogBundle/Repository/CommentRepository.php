<?php

namespace Sibers\BlogBundle\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Parameter;

/**
 * CommentRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CommentRepository extends \Doctrine\ORM\EntityRepository
{

    private $comments = array();

    public function getLatestComments($limit = 10)
    {
        $qb = $this->createQueryBuilder('c')->select('c')
                ->addOrderBy('c.id', 'DESC')->where('c.approved = 1');

        if (false === is_null($limit)) {
            $qb->setMaxResults($limit);
        }
        return $qb->getQuery()
                        ->getResult();
    }

    public function getCommentsTree($blogId)
    {
        $qb = $this->createQueryBuilder('c');
        $rootComments = $qb->where('c.blogId = :blog_id AND c.parentId IS NULL')
                ->setParameter('blog_id', $blogId)
                ->getQuery()
                ->getResult();

        $this->getNodesRecursive($rootComments);

        return $this->comments;
    }

    private function getNodesRecursive($parentNodes, $level = 1)
    {
        foreach ($parentNodes as $node) {
            if ($node->getApproved()) {
                $node->level = $level;
                $this->comments[] = $node;
                if (($children = $node->getChildren()) && $children->count() > 0) {
                    $level++;
                    $this->getNodesRecursive($children, $level);
                }
            }
        }
    }

}
