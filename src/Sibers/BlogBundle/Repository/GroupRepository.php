<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09.01.17
 * Time: 16:05
 */

namespace Sibers\BlogBundle\Repository;

use Doctrine\ORM\EntityRepository;



class GroupRepository extends EntityRepository
{

    public function getUsersByGroupName($groupName)
    {
        $group = $this->findOneBy(array('name' => $groupName));

        return $group->getUsers()->toArray();
    }


}