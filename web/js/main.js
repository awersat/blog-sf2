/*Main JS file */
var fullReload = false;
var reloadPageFlag=false;

$(function () {
    $('.full, .container .navigation a').click(actionLink);
    $('#dialog').on('loaded.bs.modal', function () {
        $('form').submit(submitForm);
    });
    $('#dialog').on('hidden.bs.modal', function () {
        $('#dialog').data('bs.modal', false);
        if(reloadPageFlag){
           reloadPageFlag=false;
           location.reload();
        }
    });

});

function actionLink()
{
    reloadContent($(this).attr('href'));
    return false;
}

function reloadContent(url)
{
    $('.cssload-loader-walk').show();
    $('#blog-content').load(url, null, function () {
        $('.cssload-loader-walk').hide();
        $('.full, .container .navigation a').click(actionLink);
        if ($('.comments-page:first').data('pageCount') > 1) {
            $('.comments-page').jscroll({
                nextSelector: 'a.next-page:last',
                contentSelector: 'article',
                debug: true
            });
        }
    });
}

function submitForm() {
    if (!fullReload) {
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: "html",
            beforeSend: function () {
                $('.cssload-loader-walk').show();
            },
            success: function (data) {
                $('.cssload-loader-walk').hide();
                if (data == 'ok') {
                    $('#dialog .modal-content .modal-body').html('\
                <p class="comment-result">Ваш комментарий успешно отправлен он станет доступен \n\
                после одобрения модератором</p>');
                } else {
                    $('#dialog .modal-content').html(data);
                    $('form').submit(submitForm);
                }
            }
        });
        return false;
    }
}